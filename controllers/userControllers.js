const User = require("../models/User");

const Product = require("../models/Product");

const bcrypt = require("bcrypt");

const auth = require("../auth");

module.exports.registerUser = (req, res) => {
  const hashedPw = bcrypt.hashSync(req.body.password, 10);
  console.log(hashedPw);

  let newUser = new User({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    email: req.body.email,
    password: hashedPw,
    mobileNo: req.body.mobileNo,
  });

  newUser
    .save()
    .then((result) => {
      console.log(result);
      return res.send(result);
    })
    .catch((error) => res.send(error));
};

module.exports.getUserDetails = (req, res) => {
  User.findById(req.user.id)
    .then((result) => res.send(result))
    .catch((error) => res.send(error));
};

module.exports.loginUser = (req, res) => {
  console.log(req.body);

  User.findOne({ email: req.body.email }).then((foundUser) => {
    if (foundUser === null) {
      return res.send({ message: "No User Found." });
    } else {
      const isPasswordCorrect = bcrypt.compareSync(
        req.body.password,
        foundUser.password
      );

      if (isPasswordCorrect) {
        return res.send({ accessToken: auth.createAccessToken(foundUser) });
      } else {
        return res.send({ message: "Incorrect Password" });
      }
    }
  });
};

module.exports.checkEmail = (req, res) => {
  User.findOne({ email: req.body.email })
    .then((result) => {
      if (result === null) {
        return res.send(false);
      } else {
        return res.send(true);
      }
    })
    .catch((error) => res.send(error));
};

module.exports.purchase = async (req, res) => {
  if (req.user.isAdmin) {
    return res.send({ message: "Action Forbidden." });
  }

  /*
		Enrollment will come in 2 steps

		First, find the user who is enrolling and update his enrollments subdocument array. We will push the courseId in the enrollments array.
		Second, find the course where we are enrolling and update its enrollees subdocument array. We will push the userId in the enrollees array.

		Since we will access 2 collections in one action, we will have to wait for the completion of the action instead of letting Javascript continue line per line.

		async and await - async keyword is added to a function to make our function asynchronous. Which means that instead of JS regular behavior of running each code line by line we will be able to to wait for the result of a function.

		TO be able to wait for the result of a function, we use the await keyword. The await keyword allows us to wait for the function to finish and get a result before proceeding.

	*/

  //return a boolean to our isUserUpdated variable to determine the result of the query and if we were able to save the courseId into our user's enrollment subdocument array.
  let isUserUpdated = await User.findById(req.user.id).then((user) => {
    //check if you found the user's document:
    //console.log(user);

    //Add the courseId in an object and push that object into the user's enrollments.
    //Because we have to follow the schema of the enrollments subdocument array:
    let newPurchase = {
      productId: req.body.productId,
    };

    //access the enrollments array from our user and push the new enrollment subdocument into the enrollments array.
    user.purchasedProducts.push(newPurchase);

    //We must save the user document and return the value of saving our document.
    //then return true IF we push the subdocument successfully
    //catch and return error message if otherwise.

    return user
      .save()
      .then((user) => true)
      .catch((err) => err.message);
  });

  //If user was able to enroll properly, isUserUpdated contains true.
  //Else, isUserUpdated will contain an error message
  //console.log(isUserUpdated);

  //Add an if statement and stop the process IF isUserUpdated DOES not contain true
  if (isUserUpdated !== true) {
    return res.send({ message: isUserUpdated });
  }

  //Find the course where we will enroll or add the user as an enrollee and return true IF we were able to push the user into the enrollees array properly or send the error message instead.
  let isProductUpdated = await Product.findById(req.body.productId).then(
    (product) => {
      //console.log(course);
      //contain the found course or the course we want to enroll in.

      //create an object to pushed into the subdocument array, enrollees.
      //We have to follow the schema of our subcocument array
      let customer = {
        userId: req.user.id,
      };

      //push the enrollee into the enrollees subdocument array of the course:
      product.customers.push(customer);

      //save the course document
      //Return true IF we were able to save and add the user as enrollee properly
      //Return an err message if we catch an error.
      return product
        .save()
        .then((product) => true)
        .catch((err) => err.message);
    }
  );

  //console.log(isCourseUpdated);

  //IF isCourseUpdated does not contain true, send the error message to the client and stop the process.
  if (isProductUpdated !== true) {
    return res.send({ message: isProductUpdated });
  }

  //Ensure that we were able to both update the user and course document to add our enrollment and enrollee respectively and send a message to the client to end the enrollment process:
  if (isUserUpdated && isProductUpdated) {
    return res.send({ message: "Thank you!" });
  }
};

module.exports.deleteUser = (req, res) => {
  User.findByIdAndDelete(req.user.id)
    .then((result) => res.send(result))
    .catch((error) => res.send(error));
};
