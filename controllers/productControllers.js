const Product = require("../models/Product");

module.exports.getAllProducts = (req, res) => {
  Product.find({})
    .then((result) => res.send(result))
    .catch((error) => res.send(error));
};

module.exports.addProduct = (req, res) => {
  let newProduct = new Product({
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
  });

  newProduct
    .save()
    .then((result) => res.send(result))
    .catch((error) => res.send(error));
};

module.exports.getActiveProducts = (req, res) => {
  Product.find({ isActive: true })
    .then((result) => res.send(result))
    .catch((error) => res.send(error));
};

module.exports.getSingleProduct = (req, res) => {
  console.log(req.params.productId);

  Product.findById(req.params.productId)
    .then((result) => res.send(result))
    .catch((error) => res.send(error));
};

module.exports.updateProduct = (req, res) => {
  console.log(req.params.productId);

  console.log(req.body);

  let update = {
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
  };

  Product.findByIdAndUpdate(req.params.productId, update, { new: true })
    .then((result) => res.send(result))
    .catch((error) => res.send(error));
};

module.exports.archiveProduct = (req, res) => {
  let update = {
    isActive: false,
  };

  Product.findByIdAndUpdate(req.params.productId, update, { new: true })
    .then((result) => res.send(result))
    .catch((error) => res.send(error));
};

module.exports.unarchiveProduct = (req, res) => {
  let update = {
    isActive: true,
  };

  Product.findByIdAndUpdate(req.params.productId, update, { new: true })
    .then((result) => res.send(result))
    .catch((error) => res.send(error));
};

module.exports.deleteProduct = (req, res) => {
  Product.findByIdAndDelete(req.params.productId)
    .then((result) => res.send(result))
    .catch((error) => res.send(error));
};
