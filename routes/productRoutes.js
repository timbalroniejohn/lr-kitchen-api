/*
	TO be able to create routes from another file to be used in our application, from here, we have to import express as well, however, we will now use another method from express to contain our routes.
	the Router() method,will us to contain our routes
*/

const express = require("express");
const router = express.Router();

const productControllers = require("../controllers/productControllers");

const auth = require("../auth");

const { verify, verifyAdmin } = auth;

router.get("/", verify, verifyAdmin, productControllers.getAllProducts);

router.post("/", verify, verifyAdmin, productControllers.addProduct);

router.get("/activeProducts", productControllers.getActiveProducts);

router.get("/getSingleProduct/:productId", productControllers.getSingleProduct);

router.put(
  "/updateProduct/:productId",
  verify,
  verifyAdmin,
  productControllers.updateProduct
);

router.delete(
  "/archiveProduct/:productId",
  verify,
  verifyAdmin,
  productControllers.archiveProduct
);

router.put(
  "/unarchiveProduct/:productId",
  verify,
  verifyAdmin,
  productControllers.unarchiveProduct
);

router.delete(
  "/deleteProduct/:productId",
  verify,
  verifyAdmin,
  productControllers.deleteProduct
);

module.exports = router;
