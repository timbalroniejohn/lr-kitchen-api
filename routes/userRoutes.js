const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers");

const auth = require("../auth");

const { verify } = auth;

router.post("/", userControllers.registerUser);

router.get("/details", verify, userControllers.getUserDetails);

router.post("/login", userControllers.loginUser);

router.post("/checkEmail", userControllers.checkEmail);

router.post("/purchase", verify, userControllers.purchase);

router.delete("/delete", verify, userControllers.deleteUser);

module.exports = router;
